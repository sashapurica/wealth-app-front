// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const baseURL = 'http://localhost:8080'; // Ruta base

const appURL = {
  createUser: baseURL + '/api/user',
  selectUser: baseURL + '/api/user/',
  login: baseURL + '/login',
  newTransaction: baseURL + '/api/movement',
  account: baseURL + '/api/account',
  accountResume: baseURL + '/api/account/resume',
  message: baseURL + '/api/message',
  sendpass: baseURL + '/api/auth/recoverypassword',
  getGender: baseURL + '/api/gender',
  getOperation: baseURL + '/api/operation',
  getRoles: baseURL + '/api/role',
  getIdentification: baseURL + '/api/identification',
  postPicture: baseURL + '/api/picture',
  getUserResume: baseURL + '/api/user/resume/'

};

export const environment = {
  production: false,
  baseURL,
  appURL
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
