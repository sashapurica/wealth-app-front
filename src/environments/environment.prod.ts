const baseURL = 'http://40.76.68.245:8082'; // Ruta base

const appURL = {
  createUser: baseURL + '/api/user',
  selectUser: baseURL + '/api/user/',
  login: baseURL + '/login',
  newTransaction: baseURL + '/api/movement',
  account: baseURL + '/api/account',
  accountResume: baseURL + '/api/account/resume',
  message: baseURL + '/api/message',
  sendpass: baseURL + '/api/auth/recoverypassword',
  getGender: baseURL + '/api/gender',
  getOperation: baseURL + '/api/operation',
  getRoles: baseURL + '/api/role',
  getIdentification: baseURL + '/api/identification',
  postPicture: baseURL + '/api/picture',
  getUserResume: baseURL + '/api/user/resume/'

};

export const environment = {
  production: true,
  baseURL,
  appURL
};
