import { Component, ViewChild, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { Router } from '@angular/router';
import { UserService } from './../../core/services/user.service';
import { User } from './../../core/interfaces/user';
import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements AfterViewInit {

  @ViewChild('sidenav', { static: false }) sidenav: MatSidenav;

  today: string;
  show: boolean;
  dataUser: User;
  admin: boolean;
  username: string;
  rol: any;
  dateNow: Date;

  private selected: any;

  constructor(
    private router: Router,
    private userService: UserService,
    private cdr: ChangeDetectorRef) {}

  private data = [{ id: 'LK', text: 'Sri Lanka' },
  { id: 'AE', text: 'United Arab Emirates' },
  { id: 'UK', text: 'United Kingdom' },
  { id: 'US', text: 'United States' }];

  ngAfterViewInit() {
    moment.locale('es');
    this.setSideNave(window);
    this.dateFormat();
    this.userMe();
    this.cdr.detectChanges();
  }


  setSideNave(target) {
    if (target.innerWidth < 960) {
      if (this.sidenav.opened) {
        this.sidenav.close();
      }
    } else {
      if (!this.sidenav.opened) {
        this.sidenav.open();
      }
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setSideNave(event.target);
  }

  @HostListener('window:ready', ['$event'])
  onLoad(event) {
    this.setSideNave(event.target);
  }

  dateFormat() {
     this.today = moment().format('ll');
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  userMe() {
    this.userService.getUserMe().subscribe(
      res => {
       this.dataUser = res;
       this.username = this.dataUser.username;
       this.dataUser.roles.forEach(element => {
         this.rol = element;
       });
       if (this.rol.id === 1) {
          this.admin = true;
       } else {
        this.admin = false;
        this.router.navigate(['/client']);
       }

       }, error => {
        console.log('Error Login :: ' + error);
        this.show = true;
      }
    );
  }

}
