import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { DetaildModule } from './detail/detail.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  MatToolbarModule, MatCardModule,
  MatIconModule, MatButtonModule, MatListModule,
  MatCheckboxModule,
  MatMenuModule,
  MatFormFieldModule, MatInputModule,
  MatSelectModule, MatTooltipModule, MatDialogModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { MatSidenavModule } from '@angular/material/sidenav';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';
import { UserService } from './core/services/user.service';
import { ClientService } from './core/services/client.service';
import { UploadService } from './core/services/upload.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { JwtInterceptor } from './core/interceptors/jwt.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    RecoverPasswordComponent,
    ChangePasswordComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    DetaildModule,
    FormsModule,
    MatToolbarModule, MatSidenavModule,
    MatListModule, MatCardModule,
    MatIconModule, MatButtonModule,
    MatCheckboxModule, MatTableModule,
    MatMenuModule, MatFormFieldModule,
    MatInputModule, MatSelectModule,
    MatTooltipModule,
    FlexLayoutModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    UserService,
    ClientService,
    UploadService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
