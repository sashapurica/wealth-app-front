import { NgModule } from '@angular/core';
import {
  MatToolbarModule, MatSidenavModule, MatCardModule,
  MatIconModule, MatButtonModule, MatListModule,
  MatCheckboxModule,
  MatMenuModule,
  MatFormFieldModule, MatInputModule,
  MatSelectModule, MatTooltipModule,
  MatDialogModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { MenuComponent,
   FooterComponent } from './index';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  imports: [MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatDialogModule,
    MatProgressSpinnerModule],
  declarations: [FooterComponent, MenuComponent],
  exports: [FooterComponent, MenuComponent]
})

export class SharedModule { }
