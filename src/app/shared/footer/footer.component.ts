import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  today: string;

  constructor() { }

  ngOnInit() {
    moment.locale('es');
    this.dateFormat();
  }

  dateFormat() {
    this.today = moment().format('ll');
 }

}
