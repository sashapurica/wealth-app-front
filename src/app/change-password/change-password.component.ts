import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from './../core/helpers/must-match.validator';
import { RecoverPasswordService } from './../core/services/recover-password.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  changePassForm: FormGroup;
  submitted = false;
  hide = true;
  msg: string;
  token: string;
  showUpdate: boolean;
  show: boolean;
  password: string;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private recoverPasswordService: RecoverPasswordService,
    private snackBar: MatSnackBar) {
    route.params.subscribe(parametros => {
      this.token = this.route.snapshot.queryParams['token'];
    });
  }

  ngOnInit() {
    this.getChangePassword(this.token);

  }

  postchangePassword(pass) {
    this.recoverPasswordService.postchangePassword(pass, this.token).subscribe(
      res => {
        this.openSnackBar('Contraseña actualizado.', 'Exitosamente');
        setTimeout(() => {
         this.router.navigate(['/login']);
        }, 2000);
      }, error => {
        this.openSnackBar('Encontramos problemas al actualizar tu contraseña, por favor comunicate con un asesor.', 'Fallo');
        this.show = false;
        console.log('Error recoverPassword :: ' + error);
      });
  }

  getChangePassword(token) {
    this.recoverPasswordService.getchangePassword(token).subscribe(
      res => {
        const result = res;
        this.showUpdate = true;
        this.changePassForm = this.fb.group({
          password: ['', Validators.required],
          confirmPassword: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmPassword')
          });
      }, error => {
        this.showUpdate = false;
        console.log('Error recoverPassword :: ' + error);
      });
  }

  get f() { return this.changePassForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.changePassForm.invalid) {
      return this.changePassForm;
    } else if (this.changePassForm.valid) {
      this.postchangePassword(this.changePassForm.value.password);
      this.show = true;
    }
  }

  cancel() {
    this.router.navigate(['/login']); // <-- go back to previous location on cancel
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
