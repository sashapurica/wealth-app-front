import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../core/services/login.service';
import { User } from './../core/interfaces/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  show: boolean;
  dataUser: User;
  rol: any;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private loginService: LoginService) { }

  ngOnInit() {

    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

  }

  login(username, password) {
    this.loginService.login(username, password).subscribe(
      (response: any) => {
        if (response.headers.get('Authorization')) {
          this.show = false;
          this.router.navigate(['/home'])
        }
      }, error => {
        console.log('Error Login :: ' + error);
        this.show = true;
      }
    );
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return this.loginForm;
    } else if (this.loginForm.valid) {
      this.login(this.loginForm.value.email, this.loginForm.value.password);
    }
  }

}
