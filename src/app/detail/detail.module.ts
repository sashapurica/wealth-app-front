import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import {
    MatToolbarModule,
    MatSidenavModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatCheckboxModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatTabsModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatDialogModule
} from '@angular/material';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HomeComponent } from './components/home/home.component';
import { InboxComponent } from './components/inbox/inbox.component';
import { ClientComponent } from './components/client/client.component';
import { UsersComponent } from './components/users/users.component';
import { TransactionsComponent } from './components/transactions/transactions.component';
import { MatTableModule } from '@angular/material/table';
import { CreateUserComponent } from './components/users/create-user/create-user.component';
import { MovementsComponent } from './components/client/movements/movements.component';
import { FindUsersComponent } from './components/users/find-users/find-users.component';
import { FindMovementsComponent } from './components/client/find-movements/find-movements.component';
import { FindClientComponent } from './components/transactions/find-client/find-client.component';
import { from } from 'rxjs';
import { SelectUserComponent } from './components/users/select-user/select-user.component';
import { UsersBalancesComponent } from './components/balance/users-balances/users-balances.component';
import { UserDetailComponent } from './components/balance/user-detail/user-detail.component';
import { DialogComponent } from './components/dialog/dialog.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        MatCardModule,
        MatIconModule,
        MatButtonModule,
        MatCheckboxModule,
        MatTableModule,
        MatMenuModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        MatTabsModule,
        MatPaginatorModule,
        FormsModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        MatDialogModule
    ],
    declarations: [
        ClientComponent,
        UsersComponent,
        TransactionsComponent,
        HomeComponent,
        InboxComponent,
        CreateUserComponent,
        MovementsComponent,
        FindUsersComponent,
        FindMovementsComponent,
        FindClientComponent,
        SelectUserComponent,
        UsersBalancesComponent,
        UserDetailComponent,
        DialogComponent
    ],
    entryComponents: [DialogComponent],
    exports: [ClientComponent, UsersComponent, TransactionsComponent, HomeComponent, InboxComponent, UsersBalancesComponent]
})

export class DetaildModule { }
