import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MessageService } from './../../../core/services/message.service';
import { Message } from './../../../core/interfaces/message';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {

  show: boolean;
  displayedColumns: string[] = ['id', 'name', 'userId', 'message', 'createdAt', 'operation', 'amount', 'icon'];
  message: string;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  data = new MatTableDataSource();

  constructor(
    private messageService: MessageService) { }

  ngOnInit() {
    this.findMessage();
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim().toLowerCase();
    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  findMessage() {
    this.messageService.selectMessageAll().subscribe(
      data => {
        let result;
        if(Object.keys(data).length === 0 ) {
          this.show = true;
          this.message = 'No existen mensaje en la bandeja.';
        } else {
          result = data;
          this.data = new MatTableDataSource<Message>(result);
          this.data.paginator = this.paginator;
          this.data.sort = this.sort;
        }
      }
    );
  }

}
