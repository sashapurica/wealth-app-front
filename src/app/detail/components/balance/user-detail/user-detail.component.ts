import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './../../../../core/services/user.service';
import { TransactionsService } from './../../../../core/services/transactions.service';
import { MessageService } from './../../../../core/services/message.service';
import { User } from './../../../../core/interfaces/user';
import { Message } from './../../../../core/interfaces/message';
import { ClientService } from './../../../../core/services/client.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Account } from './../../../../core/interfaces/account';
import { Movement } from './../../../../core/interfaces/movement';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  displayedColumns: string[] = ['id', 'amount', 'operation', 'msg', 'createdAt', 'tax'];

  id: number;
  submittedPay = false;
  userForm: FormGroup;
  payForm: FormGroup;
  showPanel: boolean;
  users: User;
  dataUser: User;
  dataAccount: Account;
  account: string;
  balance: number;
  name: string;
  dataMove = new MatTableDataSource();
  show: boolean;
  username: string;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataMovements = new MatTableDataSource();

    constructor(
      private router: Router,
      private fb: FormBuilder,
      private route: ActivatedRoute,
      private location: Location,
      private userService: UserService,
      private transactionsService: TransactionsService,
      private clientService: ClientService,
      private snackBar: MatSnackBar,
      private messageService: MessageService) {
      route.params.subscribe(parametros => {
        this.id = parametros.id;
      });
    }

  ngOnInit() {
    this.getUserAccountUser(this.id);
    this.selectUserOne(this.id);

  }

   applyFilter(filterValue: string) {
    this.dataMove.filter = filterValue.trim().toLowerCase();

    if (this.dataMove.paginator) {
      this.dataMove.paginator.firstPage();
    }
  }

  selectUserOne(id) {
    this.userService.selectUserOne(id).subscribe(
      res => {
        this.dataUser = res;
        this.username = this.dataUser.username;
        this.name = this.dataUser.firstName + ' ' + this.dataUser.lastName;
      }, error => {
        console.log('Error selectMessageOne :: ' + error);
      }
    );
  }

  getUserAccountUser(id) {
    this.clientService.accountResumeUser(id).subscribe(
      res => {
        this.dataAccount = res;
        this.account = this.dataAccount.id;
        this.dataAccount.movements.map(element => {
          if (element.messageId) {
            this.messageService.selectMessageOne(element.messageId).subscribe(
              data => {
                element.message = data;
                return element;
              }, error => {
                console.log('Error selectMessageOne :: ' + error);
              }
            );
          }
        });
        this.dataMove = new MatTableDataSource<Movement>(this.dataAccount.movements);
        this.dataMove.paginator = this.paginator;
        this.dataMove.sort = this.sort;
        if (this.dataAccount.balance == null) {
          this.balance = 0;
        } else {
          this.balance = this.dataAccount.balance;
        }
      }, error => {
        console.log('Error getUserAccount :: ' + error);
      }
    );

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  cancel() {
    this.router.navigate(['/home']); // <-- go back to previous location on cancel
  }


}
