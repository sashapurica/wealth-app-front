import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from './../../../../core/services/user.service';
import { User } from './../../../../core/interfaces/user';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-users-balances',
  templateUrl: './users-balances.component.html',
  styleUrls: ['./users-balances.component.scss']
})
export class UsersBalancesComponent implements OnInit {

  displayedColumns: string[] = [
    'account',
    'name',
    'user',
    'balance',
    'icon'
    ];

  data: any;
  clientCount: number;
  dataUser: User;
  totalBalance: number;

  // dataSource: MatTableDataSource<User>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private userService: UserService,
    private snackBar: MatSnackBar) { }

    dataUserAccount = new MatTableDataSource();

  ngOnInit() {

    this.findUserResume();

  }

  applyFilter(filterValue: string) {
    this.dataUserAccount.filter = filterValue.trim().toLowerCase();

    if (this.dataUserAccount.paginator) {
      this.dataUserAccount.paginator.firstPage();
    }
  }

  findUserResume() {
    this.userService.getUserResume().subscribe(
      data => {
        this.data = data;
        this.clientCount = data.length;
        this.totalBalance = data.reduce((prev, curr) => prev + curr.account.balance, 0);
        this.dataUserAccount = new MatTableDataSource<any>(this.data);
        this.dataUserAccount.paginator = this.paginator;
        this.dataUserAccount.sort = this.sort;
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
