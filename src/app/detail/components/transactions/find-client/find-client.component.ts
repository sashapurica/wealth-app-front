  import { Component, OnInit, ViewChild } from '@angular/core';
  import { FormBuilder, FormGroup, Validators } from '@angular/forms';
  import { Location } from '@angular/common';
  import { ActivatedRoute, Router } from '@angular/router';
  import { UserService } from './../../../../core/services/user.service';
  import { TransactionsService } from './../../../../core/services/transactions.service';
  import { MessageService } from './../../../../core/services/message.service';
  import { User } from './../../../../core/interfaces/user';
  import { Message } from './../../../../core/interfaces/message';
  import { ClientService } from './../../../../core/services/client.service';
  import { MatPaginator } from '@angular/material/paginator';
  import { MatSort } from '@angular/material/sort';
  import { MatTableDataSource } from '@angular/material/table';
  import { Account } from './../../../../core/interfaces/account';
  import { Movement } from './../../../../core/interfaces/movement';
  import { MatSnackBar } from '@angular/material/snack-bar';



  @Component({
    selector: 'app-find-client',
    templateUrl: './find-client.component.html',
    styleUrls: ['./find-client.component.scss']
  })
  export class FindClientComponent implements OnInit {

    displayedColumns: string[] = ['id', 'amount', 'operation', 'msg', 'createdAt', 'tax'];

    id: number;
    msg: number;
    payForm: FormGroup;
    debitForm: FormGroup;
    submitted = false;
    submittedPay = false;
    submittedDebit = false;
    email: string;
    name: string;
    cuenta: number;
    saldo: number;
    showInt: boolean;
    account: number;
    balance: number;
    dataUser: User;
    dataMessage: Message;
    now = Date.now();
    amountP: number;
    message: string;
    operativeP: string;
    operations: any;
    show: boolean;
    dataAccount: Account;
    operation: string;
    dataMovement: any;
    fechaMsg: string;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    dataMovements = new MatTableDataSource();

      constructor(
        private router: Router,
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private location: Location,
        private userService: UserService,
        private transactionsService: TransactionsService,
        private messageService: MessageService,
        private clientService: ClientService,
        private snackBar: MatSnackBar) {
        route.params.subscribe(parametros => {
          this.msg = parametros.msg;
        });
      }

    ngOnInit() {
      this.getOperation();
      this.getMessageOne(this.msg);
      this.payForm = this.fb.group({
        account: [this.account],
        amount: ['', Validators.required],
        operation: ['', Validators.required]
      });
    }

    navigate() {
      this.router.navigate(['/operation']);
    }

    get f() { return this.payForm.controls; }

    onSubmit() {
      this.submittedPay = true;
      if (this.payForm.invalid) {
        return this.payForm;
      } else if (this.payForm.valid) {
        if (this.balance === 0) {
          if (this.payForm.value.operation === '2') {
            this.openSnackBar('No hay saldo disponible para realizar un retiro.', 'Error');
          } else if (this.payForm.value.operation === '3') {
            this.openSnackBar('No se puede calcular interes con un balance menor a cero.', 'Error');
          } else {
            this.pay(this.payForm.value);
          }
        } else if ((this.balance !== 0) && (this.balance < this.payForm.value.amount)) {
          if (this.payForm.value.operation === '2') {
            this.openSnackBar('El monto a retirar no debe ser mayor al balance actual.', 'Error');
          }  else {
            this.pay(this.payForm.value);
          }
        } else {
          this.pay(this.payForm.value);
        }
        this.show = true;
      }
    }

  pay(data) {
      this.transactionsService.pay(data).subscribe(
        res => {
          this.dataMovement = res;
          console.log(this.dataMovement.id);
          if (this.dataMovement.id) {
            this.updateMessage(this.msg, this.dataMovement.id);
            this.openSnackBar('Transacción realizada.', 'Exitosamente');
          }
        }, error => {
          this.show = false;
          console.log('Error pay :: ' + error);
        }
      );
    }

    getOperation() {
      this.userService.getOperation().subscribe(
        res => {
          this.operations = res;
        }, error => {
          console.log('Error getOperation :: ' + error);
        }
      );
    }

    updateMessage(msg, movement) {
      this.messageService.updateStatusMessage(msg, 'PROCESS', movement).subscribe(
        res => {
          setTimeout(() => {
            this.router.navigate(['/home']);
          }, 2000);
        }, error => {
          console.log('Error updateMessage :: ' + error);
        }
      );
    }

    applyFilter(filterValue: string) {
      this.dataMovements.filter = filterValue.trim().toLowerCase();
      if (this.dataMovements.paginator) {
        this.dataMovements.paginator.firstPage();
      }
    }

    getMessageOne(msg) {
      this.messageService.selectMessageOne(msg).subscribe(
        res => {
          this.dataMessage = res;
          this.amountP = this.dataMessage.amount;
          this.message = this.dataMessage.message;
          this.operation = this.dataMessage.operation.description;
          this.dataAccount = this.dataMessage.account;
          this.dataAccount.movements.map(element => {
            if (element.messageId) {
              this.messageService.selectMessageOne(element.messageId).subscribe(
                data => {
                  element.message = data;
                  return element;
                }, error => {
                  console.log('Error selectMessageOne :: ' + error);
                }
              );
            }
          });
          this.dataMovements = new MatTableDataSource<Movement>(this.dataAccount.movements);
          this.dataMovements.paginator = this.paginator;
          this.dataMovements.sort = this.sort;
          this.account = this.dataAccount.id;
          this.balance = this.dataAccount.balance;
          this.dataUser = this.dataMessage.user;
          this.name = this.dataUser.firstName +  ' ' + this.dataUser.lastName;
          this.payForm = this.fb.group({
            account: [this.account],
            amount: ['', Validators.required],
            operation: ['', Validators.required],
          });
        }, error => {
          console.log('Error selectMessageOne :: ' + error);
        }
      );
    }

    openSnackBar(message: string, action: string) {
      this.snackBar.open(message, action, {
        duration: 2000,
      });
    }

    cancel() {
      this.router.navigate(['/home']); // <-- go back to previous location on cancel
    }

  }
