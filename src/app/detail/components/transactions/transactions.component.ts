import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Location } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from './../../../core/services/user.service';
import { User } from './../../../core/interfaces/user';
import { Account } from './../../../core/interfaces/account';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientService } from 'src/app/core/services/client.service';
import { TransactionsService } from './../../../core/services/transactions.service';
import { Movement } from 'src/app/core/interfaces/movement';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from './../../../core/services/message.service';


@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'amount', 'operation', 'msg', 'createdAt', 'tax'];

  id: number;
  submittedPay = false;
  userForm: FormGroup;
  payForm: FormGroup;
  showPanel: boolean;
  users: User;
  dataUser: User;
  dataAccount: Account;
  account: string;
  balance: number;
  name: string;
  show: boolean;
  operations: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataMove = new MatTableDataSource();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private userService: UserService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private clientService: ClientService,
    private transactionsService: TransactionsService,
    private messageService: MessageService) { }

  ngOnInit() {
    this.getOperation();
    this.findUser();
    this.userForm = this.fb.group({
     user: ['', [Validators.required]]
    });
    this.payForm = this.fb.group({
      account: [''],
      amount: ['', Validators.required],
      operation: ['', Validators.required]
    });

  }

  applyFilter(filterValue: string) {
    this.dataMove.filter = filterValue.trim().toLowerCase();
    if (this.dataMove.paginator) {
      this.dataMove.paginator.firstPage();
    }
  }

  navigate() {
    this.router.navigateByUrl('/transaction');

  }


  findUser() {
    this.userService.getUserResume().subscribe(
      data => {
        this.users = data;
      }
    );
  }

  selectUserOne(id) {
    this.userService.selectUserOne(id).subscribe(
      res => {
        this.dataUser = res;
        this.name = this.dataUser.firstName + ' ' + this.dataUser.lastName;
      }, error => {
        console.log('Error selectMessageOne :: ' + error);
      }
    );
  }

  onSubmit() {
    this.submittedPay = true;
    if (this.payForm.invalid) {
      return this.payForm;
    } else if (this.payForm.valid) {
      if (this.balance === 0) {

        if (this.payForm.value.operation === '2') {
          this.openSnackBar('No hay saldo disponible para realizar un retiro.', 'Error');
        } else if (this.payForm.value.operation === '3') {
          this.openSnackBar('No se puede calcular interes con un balance menor a cero.', 'Error');
        } else {
          this.pay(this.payForm.value);
        }
      } else if ((this.balance !== 0) && (this.balance < this.payForm.value.amount)) {
        if (this.payForm.value.operation === '2') {
          this.openSnackBar('El monto a retirar no debe ser mayor al balance actual.', 'Error');
        }  else {
          this.pay(this.payForm.value);
        }
      } else {
        this.pay(this.payForm.value);
      }
      this.show = true;
    }
  }

  pay(data) {
    this.transactionsService.pay(data).subscribe(
      res => {
        this.openSnackBar('Transacción realizada.', 'Exitosamente');
        this.show = false;
        this.payForm.reset({
          account: this.account,
          amount: ['', Validators.required],
          operation: ['', Validators.required]
        });
        this.getUserAccount(this.id);

      }, error => {
        this.show = false;
        console.log('Error pay :: ' + error);
      }
    );
  }

  clear() {
    this.showPanel = false;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  onUser(e) {
    this.id = e.value;
    this.showPanel = true;
    this.getUserAccountUser(this.id);
    this.selectUserOne(this.id);
  }

  getUserAccountUser(id) {
    this.clientService.accountResumeUser(id).subscribe(
      res => {
        this.dataAccount = res;
        this.account = this.dataAccount.id;
        this.dataAccount.movements.map(element => {
          if (element.messageId) {
            this.messageService.selectMessageOne(element.messageId).subscribe(
              data => {
                element.message = data;
                return element;
              }, error => {
                console.log('Error selectMessageOne :: ' + error);
              }
            );
          }
        });
        this.dataMove = new MatTableDataSource<Movement>(this.dataAccount.movements);
        this.dataMove.paginator = this.paginator;
        this.dataMove.sort = this.sort;
        if (this.dataAccount.balance == null) {
          this.balance = 0;
        } else {
          this.balance = this.dataAccount.balance;
        }
        this.payForm = this.fb.group({
          account: [this.account],
          amount: ['', Validators.required],
          operation: ['', Validators.required]
        });
      }, error => {
        console.log('Error getUserAccount :: ' + error);
      }
    );

  }

  getUserAccount(id) {
    this.clientService.accountResumeUser(id).subscribe(
      res => {
        this.dataAccount = res;
        this.account = this.dataAccount.id;
        this.dataMove = new MatTableDataSource<Movement>(this.dataAccount.movements);
        this.dataMove.paginator = this.paginator;
        this.dataMove.sort = this.sort;
        if (this.dataAccount.balance == null) {
          this.balance = 0;
        } else {
          this.balance = this.dataAccount.balance;
        }
      }, error => {
        console.log('Error getUserAccount :: ' + error);
      }
    );

  }

  getOperation() {
    this.userService.getOperation().subscribe(
      res => {
        this.operations = res;
      }, error => {
        console.log('Error getOperation :: ' + error);
      }
    );
  }

}
