import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MessageService } from './../../../../core/services/message.service';
import { ClientService } from './../../../../core/services/client.service';
import { Account } from './../../../../core/interfaces/account';
import { Movement } from './../../../../core/interfaces/movement';

@Component({
  selector: 'app-find-movements',
  templateUrl: './find-movements.component.html',
  styleUrls: ['./find-movements.component.scss']
})
export class FindMovementsComponent implements OnInit {

  account: number;
  balance: number;
  dataUser: Account;
  message: string;
  show: boolean;
  dataMove = new MatTableDataSource();

  displayedColumns: string[] = ['id', 'amount', 'operation', 'msg', 'createdAt', 'tax'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private clientService: ClientService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.getUserAccount();
  }

  applyFilter(filterValue: string) {
    this.dataMove.filter = filterValue.trim().toLowerCase();
    if (this.dataMove.paginator) {
      this.dataMove.paginator.firstPage();
    }
  }

  getUserAccount() {
    this.clientService.accountResume().subscribe(
      res => {
        if (Object.keys(res).length === 0 ) {
          this.show = true;
          this.message = 'No se encontraron movimientos para la fecha';
        } else {
        this.dataUser = res;
        this.dataUser.movements.map(element => {
          if (element.messageId) {
            this.messageService.selectMessageOne(element.messageId).subscribe(
              data => {
                element.message = data;
                return element;
              }, error => {
                console.log('Error selectMessageOne :: ' + error);
              }
            );
          }
        });
        this.dataMove = new MatTableDataSource<Movement>(this.dataUser.movements);
        this.dataMove.paginator = this.paginator;
        this.dataMove.sort = this.sort;
        }
      }, error => {
        console.log('Error getUserAccount :: ' + error);
      }
    );

  }

}
