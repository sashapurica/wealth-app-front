import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from './../../../core/services/user.service';
import { ClientService } from './../../../core/services/client.service';
import { User } from './../../../core/interfaces/user';
import { Account } from './../../../core/interfaces/account';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  account: number;
  balance: number;
  dataUser: User;
  dataAccount: Account;
  id: number;

  constructor(
    private router: Router,
    private userService: UserService,
    private clientService: ClientService
  ) { }

  ngOnInit() {
    this.getUserAccount();
    this.userMe();
  }

  getUserAccount() {
    this.clientService.accountResume().subscribe(
      res => {
        this.dataAccount = res;
        this.account = this.dataAccount.id;
        if (this.dataAccount.balance == null) {
          this.balance = 0;
        } else {
          this.balance = this.dataAccount.balance;
        }

      }
    );

  }

  userMe() {
    this.userService.getUserMe().subscribe(
      res => {
        this.dataUser = res;
        this.id = this.dataUser.id;
      }, error => {
        console.log('Error Login :: ' + error);
      }
    );
  }

}
