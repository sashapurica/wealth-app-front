import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from './../../../../core/services/message.service';
import { UserService } from './../../../../core/services/user.service';
import { User } from './../../../../core/interfaces/user';
import { Account } from './../../../../core/interfaces/account';
import { ClientService } from './../../../../core/services/client.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-movements',
  templateUrl: './movements.component.html',
  styleUrls: ['./movements.component.scss']
})
export class MovementsComponent implements OnInit {

  operationForm: FormGroup;
  submitted = false;
  data: any;
  account: number;
  balance: number;
  dataUser: User;
  id: number;
  amount: number;
  operations: any;
  opt: any;
  show: boolean;
  dataAccount: Account;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private messageService: MessageService,
    private userService: UserService,
    private clientService: ClientService,
    private snackBar: MatSnackBar) {
    route.params.subscribe(parametros => {
      this.id = parametros.id;
    });
  }

  ngOnInit() {
    this.getOperation();
    this.getUserAccount();
    this.operationForm = this.fb.group({
      account: [this.account],
      amount: [this.amount],
      quantity: ['', Validators.required],
      message: ['', Validators.required],
      operation: ['', Validators.required]
    });
  }

  navigate() {
    this.router.navigate(['/home']);
  }

  get f() { return this.operationForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.operationForm.invalid) {
      return this.operationForm;
    } else if (this.operationForm.valid) {
      this.sendMessage(JSON.stringify(this.operationForm.value));
    }
  }

  sendMessage(data) {
    this.messageService.sendMessage(data).subscribe(
      response => {
        this.openSnackBar('Se envió el mensaje.', 'Exitosamente');
        this.show = true;
        setTimeout(() => {
          this.router.navigate(['/client']);
         }, 3000);
      }, error => {
        this.show = false;
        console.log('Error sendMessage :: ' + error);
      });
  }

  getUserOne(id) {
    this.userService.selectUserOne(id).subscribe(
      res => {
        this.dataUser = res;
        this.operationForm = this.fb.group({
          account: [this.account],
          amount: ['', Validators.required],
          userId: [this.dataAccount.userId],
          status: ['ACTIVE'],
          message: ['', Validators.required],
          operation: ['', Validators.required]
        });
      }, error => {
        console.log('Error getUserOne :: ' + error);
      }
    );

  }

  getOperation() {
    this.userService.getOperation().subscribe(
      res => {
        console.log(res);
        this.opt = res;
        this.opt.splice(2);
        this.operations = this.opt;
      }, error => {
        console.log('Error getOperation :: ' + error);
      }
    );
  }

  getUserAccount() {
    this.clientService.accountResume().subscribe(
      res => {
        this.dataAccount = res;
        this.account = this.dataAccount.id;
        this.getUserOne(this.dataAccount.userId);
        if (this.dataAccount.balance == null) {
          this.balance = 0;
        } else {
          this.balance = this.dataAccount.balance;
        }
      }, error => {
        console.log('Error getUserAccount :: ' + error);
      }
    );

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  cancel() {
    this.router.navigate(['/client']); // <-- go back to previous location on cancel
  }


}
