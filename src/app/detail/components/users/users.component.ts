import { Component, OnInit } from '@angular/core';
import { UserService } from './../../../core/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {


  constructor(private userService: UserService) { }

  data: any;
  operations: any;

  ngOnInit() {
  }

  getOperation() {
    this.userService.getOperation().subscribe(
      res => {
        this.operations = res;
      }
    );
    }

}
