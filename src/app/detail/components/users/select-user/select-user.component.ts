import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from './../../../../core/services/user.service';
import { User } from './../../../../core/interfaces/user';
import { Message } from './../../../../core/interfaces/message';
import { MessageService } from './../../../../core/services/message.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';
import { DialogComponent } from './../../dialog/dialog.component';
import { UploadService } from './../../../../core/services/upload.service';

@Component({
  selector: 'app-select-user',
  templateUrl: './select-user.component.html',
  styleUrls: ['./select-user.component.scss']
})
export class SelectUserComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  id: number;
  msg: number;
  email: string;
  name: string;
  cuenta: number;
  saldo: number;
  showInt: boolean;
  account: number;
  balance: number;
  dataUser: User;
  dataMessage: Message;
  message: string;
  rol: string;
  roles: any;
  operations: any;
  show: boolean;
  picture: string;
  zip: number;
  address: string;
  phone: number;
  documentType: string;
  city: string;
  state: string;
  status: string;
  document: number;
  genders: any;
  rolesEdit: any;
  documentTypes: any;
  gendersEdit: any;
  documentTypesEdit: any;
  userOpe: string;
  company: string;
  firstName: string;
  lastName: string;
  showEditUser: boolean;
  documentFor: any;
  role: any;
  gender: number;
  genderView: string;
  file: any;

  constructor(private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private userService: UserService,
              private messageService: MessageService,
              private snackBar: MatSnackBar,
              public dialog: MatDialog,
              private uploadService: UploadService) {
    route.params.subscribe(parametros => {
      this.id = parametros.id;
    });
  }

  ngOnInit() {
    this.selectUserOne(this.id);
    this.getRole();
    this.getGender();
    this.getIdentication();

    this.registerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      gender: [''],
      document: ['', [Validators.required]],
      documentType: [''],
      picture: [''],
      address: ['', [Validators.required]],
      company: [''],
      zip: ['', [Validators.required, Validators.minLength(8)]],
      status: ['ACTIVE'],
      role: ['', [Validators.required]]
    });
  }

  selectUserOne(id: number) {
    this.userService.selectUserOne(id).subscribe(
      res => {
        this.dataUser = res;
        this.firstName = this.dataUser.firstName;
        this.lastName = this.dataUser.lastName;
        this.email = this.dataUser.email;
        this.zip = this.dataUser.zip;
        this.address = this.dataUser.address;
        this.document = this.dataUser.document;
        this.documentType = this.dataUser.documentType;
        this.dataUser.roles.forEach(element => {
          this.rol = element.description;
          this.role = element;
        });

        this.phone = this.dataUser.phone;
        this.state = this.dataUser.state;
        this.city = this.dataUser.city;
        this.company = this.dataUser.company;
        this.status = this.dataUser.status;
        this.name = this.dataUser.firstName + ' ' + this.dataUser.lastName;
        this.picture = this.dataUser.picture;
        this.gender = this.dataUser.gender.id;
        this.genderView = this.dataUser.gender.description;
        


      }, error => {
        console.log('Error selectMessageOne :: ' + error);
      }
    );
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return this.registerForm;
    } else if (this.registerForm.valid) {

      this.show = true;
      this.updateDataUser(this.registerForm.value);

    }
  }

  finalUpload(file) {
    this.uploadService.subirArchivo(file).subscribe(response => {
      this.picture = response.message;
      this.registerForm.controls.picture.setValue(response.message);
    },
      error => {
        console.error(error);
      });
  }

  handleError(error: any) {
    return error;

  }

  onFileChange($event) {
    this.file = $event.target.files[0]; // <--- File Object for future use.
    const file = $event.target.files[0]; // <--- File Object for future use.
    this.finalUpload(file);
  }

  updateDataUser(data) {
    this.userService.updateDataUser(data, this.id).subscribe(res => {
      this.openSnackBar('Usuario Actualizado.', 'Exitosamente');
      this.show = false;
      this.selectUserOne(this.id);
      this.showEditUser = false;
    }, error => {
      this.show = false;
      console.error(error);
    });
  }

  deleteUser() {
    const status = { status: 'INACTIVE' };
    this.userService.updateUser(status, this.id).subscribe(
      data => {
        this.status = 'INACTIVE';
        }
    );
  }

  enableUser() {
    const status = { status: 'ACTIVE' };
    this.userService.updateUser(status, this.id).subscribe(
      data => {
        this.status = 'ACTIVE';
        }
    );
  }

  getGender() {
    this.userService.getGender().subscribe(
      res => {
        this.gendersEdit = res;
      }
    );
  }

  getIdentication() {
    this.userService.getIdentication().subscribe(
      res => {
        this.documentTypesEdit = res;
      }
    );
  }

  getRole() {
    this.userService.getRole().subscribe(
      res => {
        this.rolesEdit = res;
      }
    );
  }

  cancel() {
    this.router.navigate(['/user']); // <-- go back to previous location on cancel
  }

  openDialog(e: string): void {
    if (e === 'ACTIVE' ) {
        this.userOpe = 'activar';
        } else {
          this.userOpe = 'desactivar';
        }

    const dialogRef = this.dialog.open(DialogComponent, {
      width: '450px',
      data: {message: '¿Esta seguro que desea ' + this.userOpe + ' el usuario?'}
    });

    dialogRef.afterClosed().subscribe(result => {
    if (result) {
      if (this.userOpe === 'activar' ) {
        this.enableUser();
        } else {
          this.deleteUser();
        }
     } else if (typeof result === 'undefined' ) {
      console.log('cancel');
     }
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


  showEdit( ) {
    if (this.showEditUser) {
      this.showEditUser = false;
    } else {
      this.showEditUser = true;
      this.setvalueEdit();
    }
  }

  setvalueEdit() {
        this.registerForm.controls.email.setValue(this.email);
        this.registerForm.controls.phone.setValue(this.phone);
        this.registerForm.controls.document.setValue(this.document);
        this.registerForm.controls.city.setValue(this.city);
        this.registerForm.controls.zip.setValue(this.zip);
        this.registerForm.controls.address.setValue(this.address);
        this.registerForm.controls.state.setValue(this.state);
        this.registerForm.controls.company.setValue(this.company);
        this.registerForm.controls.firstName.setValue(this.firstName);
        this.registerForm.controls.lastName.setValue(this.lastName);
        this.registerForm.controls.role.setValue(this.role.id);
        this.registerForm.controls.gender.setValue(this.gender);
        this.registerForm.controls.documentType.setValue(this.documentType);
        this.registerForm.controls.picture.setValue(this.dataUser.picture);
  }

}
