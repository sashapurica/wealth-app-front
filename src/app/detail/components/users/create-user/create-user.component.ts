import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './../../../../core/services/user.service';
import { UploadService } from './../../../../core/services/upload.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  file: any;
  load: boolean;
  show: boolean;
  genders: any;
  roles: any;
  documentTypes: any;
  picture: string;

  constructor(
    private userService: UserService,
    private router: Router,
    private fb: FormBuilder,
    private uploadService: UploadService,
    private snackBar: MatSnackBar
  ) {

  }

  ngOnInit() {
    this.getRole();
    this.getGender();
    this.getIdentication();
    this.registerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      document: ['', [Validators.required]],
      documentType: ['', [Validators.required]],
      picture: ['', [Validators.required]],
      address: ['', [Validators.required]],
      company: [''],
      zip: ['', [Validators.required, Validators.minLength(8)]],
      status: ['ACTIVE'],
      role: [this.roles, [Validators.required]]
    });
  }

  postCreateUser(data) {
    this.userService.createUser(data).subscribe(res => {
      this.openSnackBar('Usuario creado.', 'Exitosamente');
      this.load = false;

      setTimeout(() => {
        this.router.navigate(['/user']);
      }, 3000);
    }, error => {
      this.load = false;
      this.show = false;
      console.error(error.status);
      if (error.status === 500) {
        this.openSnackBar('Email ya existe.', 'Error');
      }
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return this.registerForm;
    } else if (this.registerForm.valid) {
      this.load = true;
      this.show = true;
      this.postCreateUser(JSON.stringify(this.registerForm.value));

    }
  }

  finalUpload(file) {
    this.uploadService.subirArchivo(file).subscribe(response => {
      this.picture = response.message;
      this.registerForm.controls.picture.setValue(response.message);
    },
      error => {
        console.error(error);
      });
  }

  handleError(error: any) {
    return error;

  }

  onFileChange($event) {
    this.file = $event.target.files[0]; // <--- File Object for future use.
    const file = $event.target.files[0]; // <--- File Object for future use.
    this.finalUpload(file);
  }


  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  getGender() {
    this.userService.getGender().subscribe(
      res => {
        this.genders = res;
      }
    );
  }

  getIdentication() {
    this.userService.getIdentication().subscribe(
      res => {
        this.documentTypes = res;
      }
    );
  }

  getRole() {
    this.userService.getRole().subscribe(
      res => {
        this.roles = res;
      }
    );
  }

  cancel() {
    this.router.navigate(['/user']); // <-- go back to previous location on cancel
  }

}
