import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from './../../../../core/services/user.service';
import { User } from './../../../../core/interfaces/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';
import { DialogComponent } from './../../dialog/dialog.component';

@Component({
  selector: 'app-find-users',
  templateUrl: './find-users.component.html',
  styleUrls: ['./find-users.component.scss']
})
export class FindUsersComponent implements OnInit {

  userOpe: string;

  displayedColumns: string[] = [
    'id',
    'firstName',
    'lastName',
    'email',
    'status'
  ];

  // dataSource: MatTableDataSource<User>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private userService: UserService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog) { }

  data = new MatTableDataSource();

  ngOnInit() {

    this.findUser();

  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim().toLowerCase();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  findUser() {
    this.userService.selectUserAll().subscribe(
      data => {
        let result;
        result = data;
        this.data = new MatTableDataSource<User>(result);
        this.data.paginator = this.paginator;
        this.data.sort = this.sort;
      }
    );
  }

  deleteUser(e) {
    const status = {status: 'INACTIVE'};
    this.userService.updateUser(status, e.id).subscribe(
       data => {
          e.status = 'INACTIVE';
       }
     );

  }

  enableUser(e) {
    const status = {status: 'ACTIVE'};
    this.userService.updateUser(status, e.id).subscribe(
       data => {
         e.status = 'ACTIVE';
        }
     );

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


  openDialog(e, msg: string): void {

    if (msg === 'ACTIVE' ) {
        this.userOpe = 'activar';
      } else {
        this.userOpe = 'desactivar';
    }

    const dialogRef = this.dialog.open(DialogComponent, {
      width: '450px',
      data: {message: '¿Esta seguro que desea ' + this.userOpe + ' el usuario?'}
    });

    dialogRef.afterClosed().subscribe(result => {
    if (result) {
      if (this.userOpe === 'activar' ) {
         this.enableUser(e);
        } else {
         this.deleteUser(e);
        }
     } else if (typeof result === 'undefined' ) {
      console.log('cancel');
     }
    });
  }


}
