import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './detail/components/home/home.component';
import { UsersComponent } from './detail/components/users/users.component';
import { FindClientComponent } from './detail/components/transactions/find-client/find-client.component';
import { TransactionsComponent } from './detail/components/transactions/transactions.component';
import { ClientComponent } from './detail/components/client/client.component';
import { CreateUserComponent } from './detail/components/users/create-user/create-user.component';
import { LoginComponent } from './login/login.component';
import { MovementsComponent } from './detail/components/client/movements/movements.component';
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SelectUserComponent } from './detail/components/users/select-user/select-user.component';
import { UsersBalancesComponent } from './detail/components/balance/users-balances/users-balances.component';
import { UserDetailComponent } from './detail/components/balance/user-detail/user-detail.component';
import { AuthGuard } from './core/guards/auth.guard';
import { RoleGuard } from './core/guards/role.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'recoverPassword', component: RecoverPasswordComponent },
  { path: 'changePassword', component: ChangePasswordComponent },
  {
    path: '', component: DashboardComponent, canActivate: [AuthGuard],
    children: [
      { path: 'home', component: HomeComponent, canActivate: [RoleGuard], data: {role: 'admin'} },
      { path: 'user', component: UsersComponent, canActivate: [RoleGuard], data: {role: 'admin'} },
      { path: 'client', component: ClientComponent, canActivate: [RoleGuard], data: {role: 'user'} },
      { path: 'transaction/:msg', component: FindClientComponent, canActivate: [RoleGuard], data: {role: 'admin'} },
      { path: 'transaction', component: TransactionsComponent, canActivate: [RoleGuard], data: {role: 'admin'} },
      { path: 'usuario/crear', component: CreateUserComponent, canActivate: [RoleGuard], data: {role: 'admin'} },
      { path: 'usuario/consulta/:id', component: SelectUserComponent, canActivate: [RoleGuard], data: {role: 'admin'} },
      { path: 'cliente/operacion/:id', component: MovementsComponent, canActivate: [RoleGuard], data: {role: 'user'} },
      { path: 'balance', component: UsersBalancesComponent, canActivate: [RoleGuard], data: {role: 'admin'} },
      { path: 'balance/user/:id', component: UserDetailComponent, canActivate: [RoleGuard], data: {role: 'admin'} },
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
