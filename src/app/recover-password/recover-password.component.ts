import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RecoverPasswordService } from './../core/services/recover-password.service';
import { of, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {

  recoverPassForm: FormGroup;
  submitted = false;
  msg: string;
  show: boolean;
  email: string;
  load: boolean;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private recoverPasswordService: RecoverPasswordService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {

    this.recoverPassForm = this.fb.group({
      email: ['', Validators.required]
    });
  }

  recoverPassword(email) {
    this.recoverPasswordService.recoverPassword(email).subscribe(
      res => {
        this.msg = 'En minutos te llegara un mail, para que puedas recuperar tu contraseña.';
        this.openSnackBar('¡Solicitud enviada!', 'Exitosamente');
        this.load = false;
        this.navigate();
      }, error => {
        this.load = false;
        this.msg = 'El email no se encuentra en nuestra base de datos.';
        console.log('Error recoverPassword :: ' + error);
        this.show = false;
      });
  }

  navigate() {
    this.router.navigate(['/login']);
  }

  get f() { return this.recoverPassForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.recoverPassForm.invalid) {
      return this.recoverPassForm;
    } else if (this.recoverPassForm.valid) {
      this.recoverPassword(this.recoverPassForm.value.email);
      this.show = true;
      this.load = true;
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  cancel() {
    this.router.navigate(['/login']); // <-- go back to previous location on cancel
  }


}
