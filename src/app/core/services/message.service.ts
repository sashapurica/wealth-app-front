import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class MessageService {
  constructor(
    private http: HttpClient
  ) { }

  public sendMessage(data) {
    return this.http.post(environment.appURL.message, data);
  }

  public selectMessageAll() {
    return this.http.get(environment.appURL.message);
  }

  public selectMessageOne(id){
    return this.http.get(environment.appURL.message + '/' + id);
  }

  public updateStatusMessage(id, newStatus, movement) {
    const option = {
      status: newStatus,
      movementId: movement
    };
    return this.http.put(environment.appURL.message + '/' + id, option);
  }

}
