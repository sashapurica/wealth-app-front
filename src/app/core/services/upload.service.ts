import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(
    private http: HttpClient
  ) { }

  subirArchivo(file): any {
    const formData = new FormData();
    formData.append('picture', file);
    formData.append('fileName', file.name);
    return this.http.post(environment.appURL.postPicture, formData);
  }

}
