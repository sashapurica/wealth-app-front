import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {
  constructor(
    private http: HttpClient
  ) { }

  public pay(data) {
    return this.http.post(environment.appURL.newTransaction, data);
  }

}
