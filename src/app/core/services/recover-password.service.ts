import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecoverPasswordService {

  constructor(private http: HttpClient) { }

  recoverPassword(email) {
    return this.http.post(environment.appURL.sendpass, {
      email,
    }, { observe: 'response' });
  }

  getchangePassword(token) {

    return this.http.get(environment.appURL.sendpass + '/' + token);
  }

  postchangePassword(password, token) {
    return this.http.post(environment.appURL.sendpass + '/' + token, {
      password,
    }, { observe: 'response' });
  }

}
