import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { map } from 'rxjs/operators';
import { User } from '../interfaces/user';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private isLoggedIn = false;

  private token;

  private user: User;

  constructor(private http: HttpClient, private userService: UserService) { }

  login(email, pass, remember = false) {
    return this.http.post(environment.appURL.login, {
      username: email,
      password: pass,
    }, { observe: 'response' })
      .pipe(
        map(response => {
          this.token = response.headers.get('Authorization');
          if (remember) {
            localStorage.setItem('token', this.token);
          }
          this.userService.getUserMe().subscribe( res => this.user = res );
          this.isLoggedIn = true;
          return response;
        })
      );
  }

  public isLogged() {
    return this.isLoggedIn;
  }

  public getUser() {
    return this.user;
  }

  public getToken() {
    return this.token;
  }

}
