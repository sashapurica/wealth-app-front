import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(
    private http: HttpClient
  ) { }

  public account() {
    return this.http.get(environment.appURL.account);

  }

  public getMovements() {
    return this.http.get(environment.appURL.newTransaction);
  }

  public accountResume() {
    return this.http.get(environment.appURL.accountResume);
  }

  public accountResumeUser(id) {
    return this.http.get(environment.appURL.accountResume + '/' + id);
  }

}
