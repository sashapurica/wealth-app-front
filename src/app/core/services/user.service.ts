import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  public createUser(data) {
    return this.http.post(environment.appURL.createUser, data);
  }

  public selectUserAll() {
    return this.http.get(environment.appURL.selectUser);

  }

  public selectUserOne(id) {
    return this.http.get(environment.appURL.selectUser + id);
  }

  public updateUser(data, id) {
    return this.http.put(environment.appURL.selectUser + id + '/status', data);
  }

  public updateDataUser(data, id) {
    return this.http.put(environment.appURL.selectUser + id, data);
  }

  public getGender() {
    return this.http.get(environment.appURL.getGender);
  }

  public getOperation() {
    return this.http.get(environment.appURL.getOperation);

  }

  public getRole() {
    return this.http.get(environment.appURL.getRoles);

  }

  public getIdentication() {
    return this.http.get(environment.appURL.getIdentification);

  }

   public getUserMe(): Observable<User> {
    return this.http.get(environment.appURL.createUser + '/me');
  }

  public getUserResume(): Observable<any> {
    return this.http.get(environment.appURL.getUserResume);
  }

}
