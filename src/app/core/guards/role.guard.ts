import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class RoleGuard implements CanActivate {
    constructor(
        private router: Router,
        private userService: UserService
    ) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.userService.getUserMe().pipe(
            map(
                res => {
                    const user = res;
                    const found = user.roles.find(role => role.name === next.data.role);
                    if (found) {
                        return true;
                    } else {
                        this.router.navigate(['/client']);
                        return true;
                    }
                }
            )
        );
    }
}
