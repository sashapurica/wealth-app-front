import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private loginService: LoginService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (request.url.indexOf('picture') !== -1) {
            return next.handle(request);
        }
        const token = this.loginService.getToken();
        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `${token}`,
                    'Content-Type': 'application/json'
                }
            });
        }

        return next.handle(request);
    }
}