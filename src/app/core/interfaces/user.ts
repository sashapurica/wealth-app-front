export interface User {
    id?;
    account?;
    firstName?;
    lastName?;
    document?;
    documentType?;
    email?;
    phone?;
    address?;
    company?;
    city?;
    state?;
    zip?;
    username?;
    password?;
    roles?: any[];
    picture?;
    gender?;
    status?;
}
