export interface Message {
    account?;
    amount?;
    id?;
    message?;
    operation?;
    userId?;
    user?;
    createdAt?;
}
