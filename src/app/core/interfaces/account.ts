export interface Account {
    id?;
    account?;
    amount?;
    operation?;
    interest?;
    balance?;
    previousBalance?;
    dateOpe?;
    userId?;
    tax?;
    movements?;


}
