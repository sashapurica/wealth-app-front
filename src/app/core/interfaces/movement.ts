export interface Movement {
    balance?;
    id?;
    movements?;
    userId?;
    account?;
    amount?;
    createdAt?;
    operation?;
    tax?;
    updatedAt?;
    message?;

}
